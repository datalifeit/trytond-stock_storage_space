# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import unittest
import doctest
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import doctest_setup, doctest_teardown


class StorageSpaceTestCase(ModuleTestCase):
    """Test module"""
    module = 'stock_storage_space'

    def setUp(self):
        super(StorageSpaceTestCase, self).setUp()


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(StorageSpaceTestCase))
    suite.addTests(doctest.DocFileSuite('scenario_storage_space.rst',
           setUp=doctest_setup, tearDown=doctest_teardown, encoding='utf-8',
           optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    return suite
