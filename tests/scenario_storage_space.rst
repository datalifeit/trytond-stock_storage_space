=============
Storage space
=============

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.exceptions import UserError, UserWarning
    >>> from proteus import config, Model, Wizard
    >>> today = datetime.date.today()

Create database::

    >>> config = config.set_trytond()
    >>> config.pool.test = True

Install stock storage space Module::

    >>> Module = Model.get('ir.module.module')
    >>> module, = Module.find([('name', '=', 'stock_storage_space')])
    >>> module.click('install')
    >>> Wizard('ir.module.module.install_upgrade').execute('upgrade')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Reload the context::

    >>> User = Model.get('res.user')
    >>> config._context = User.get_preferences(True, config.context)

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> ProductCategory = Model.get('product.category')
    >>> category = ProductCategory(name='Category 1')
    >>> category.save()
    >>> template = ProductTemplate()
    >>> product = Product()
    >>> template.name = 'Tray'
    >>> template.type = 'goods'
    >>> template.category = category
    >>> template.default_uom = unit
    >>> template.list_price = Decimal(10)
    >>> template.cost_price = Decimal(5)
    >>> template.occupy_space = True
    >>> template.save() # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ...
    >>> template.length = Decimal(0.5)
    >>> meter, = ProductUom.find([('name', '=', 'Meter')], limit=1)
    >>> template.length_uom = meter
    >>> template.save()
    >>> product.template = template
    >>> product.save()

Create locations::

    >>> Location = Model.get('stock.location')
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> input_loc, = Location.find([('code', '=', 'IN')])
    >>> storage_loc.control_space = True
    >>> storage_loc.save() # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ...
    >>> storage_loc.length = Decimal(20)
    >>> storage_loc.length_uom = meter
    >>> storage_loc.overload_behavior = 'warn'
    >>> storage_loc.save()
    >>> storage_loc.space
    20.0
    >>> storage_loc.available_space
    20.0

Create moves::

    >>> StockMove = Model.get('stock.move')
    >>> move = StockMove()
    >>> move.product = product
    >>> move.uom =unit
    >>> move.quantity = 41
    >>> move.from_location = input_loc
    >>> move.to_location = storage_loc
    >>> move.company = company
    >>> move.planned_date = today
    >>> move.unit_price = Decimal('1')
    >>> move.currency = company.currency
    >>> move.save() # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserWarning: ...
    >>> move.quantity = 40
    >>> move.save()
    >>> move.click('assign')
    >>> move.click('do')
    >>> storage_loc.reload()
    >>> storage_loc.available_space
    0.0
