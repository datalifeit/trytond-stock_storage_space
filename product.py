# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval

__all__ = ['Template', 'Product']

__metaclass__ = PoolMeta


class Template:
    __name__ = 'product.template'

    occupy_space = fields.Boolean('Occupy space',
                                  help='Determines if product occupies space in storage locations')

    @classmethod
    def __setup__(cls):
        super(Template, cls).__setup__()
        cls._extend_measure_field('length')

    @staticmethod
    def default_occupy_space():
        return False

    @classmethod
    def _extend_measure_field(cls, field):
        f = getattr(cls, field, None)
        if not f:
            return
        req = Eval('occupy_space')
        if not f.states.get('required'):
            f.states['required'] = req
        else:
            f.states['required'] = f.states['required'] | req
        f.depends.append('occupy_space')


class Product:
    __name__ = 'product.product'

    @classmethod
    def get_products_occupy_space(cls):
        pool = Pool()
        Product = pool.get('product.product')

        products = Product.search([('occupy_space', '=', True)])
        return [p.id for p in products] or []

    def get_space(self, quantity, uom=None):
        pool = Pool()
        UOM = pool.get('product.uom')
        ModelData = pool.get('ir.model.data')
        meter = UOM(ModelData.get_id('product', 'uom_meter'))

        if not getattr(self, 'occupy_space', False):
            return float(0)
        qty = UOM.compute_qty(uom if uom else self.default_uom,
                              quantity, self.default_uom)
        return UOM.compute_qty(self.length_uom, self.length, meter) * qty

    def get_quantity_from_space(self, space):
        pool = Pool()
        UOM = pool.get('product.uom')
        ModelData = pool.get('ir.model.data')
        meter = UOM(ModelData.get_id('product', 'uom_meter'))

        if not getattr(self, 'occupy_space', False):
            return space
        return int(space / UOM.compute_qty(self.length_uom, self.length, meter))